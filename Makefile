TAG?=$(shell git rev-list HEAD --max-count=1 --abbrev-commit)
export TAG

pack:
	docker build -t gcr.io/lorenzswtde/rangebooking-frontend:$(TAG) .

upload:
	docker push gcr.io/lorenzswtde/rangebooking-frontend:$(TAG)
